=====
Polls
=====

.. image:: https://api.travis-ci.org/mateitrusca/django-pills.png
   :target: https://travis-ci.org/mateitrusca/django-pills

Polls is a simple Django app to conduct Web-based pills. For each
question, visitors can choose between a fixed number of answers.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "pills" to your INSTALLED_APPS setting like this::

      INSTALLED_APPS = (
          ...
          'pills',
      )

2. Include the pills URLconf in your project urls.py like this::

      url(r'^pills/', include('pills.urls')),

3. Run `python manage.py syncdb` to create the pills models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
  to create a poll (you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/pills/ to participate in the poll.
